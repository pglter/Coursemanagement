package models.classes;

import models.interfaces.Course;
import models.interfaces.File;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by mikegoebel on 13.07.16.
 */
@Entity
@Table(name = "file")
public class FileHibernateImpl implements File {


    private int fileId;

    private String filename;
    private String kategorie;
    private String kommentar;
    private Course course;
    private String path;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = CourseHibernateImpl.class)
    @JoinColumn(name = "courseId")
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public FileHibernateImpl() {

    }

    // Konstruktor anpassen. Ohne ID?
    public FileHibernateImpl(Course course, String filename, String kategorie, String kommentar, String path) {
        this.filename = filename;
        this.kategorie = kategorie;
        this.kommentar = kommentar;
        this.path = path;
        this.course = course;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getKategorie() {
        return kategorie;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
