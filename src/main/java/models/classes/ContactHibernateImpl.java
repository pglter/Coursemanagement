package models.classes;

import models.interfaces.Contact;

import javax.persistence.*;
import java.util.Comparator;

/**
 * Created by mikegoebel on 13.07.16.
 */
@Entity
@Table(name = "contact")
public class ContactHibernateImpl implements Contact, Comparable<Contact>, Comparator<Contact> {

    private int contactID;
    private String vorname;
    private String name;
    private String email;


   public ContactHibernateImpl(){

   }
    public ContactHibernateImpl(String vorname, String name, String email) {
        this.vorname = vorname.trim();
        this.name = name.trim();
        this.email = email.toLowerCase();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getContactID() {
        return contactID;
    }

    public void setContactID(int contactID) {
        this.contactID = contactID;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString(){
        String result=email;
        if(!vorname.isEmpty()){
            result+=" ("+vorname+")";
            if (!name.isEmpty()) {
                result=result.substring(0,result.length()-1)+" "+name+")";
            }
        }
        return result;
    }

    @Override
    public int compareTo(Contact o) {
        return email.compareTo(o.getEmail());
    }
    @Override
    public int compare(Contact o1,Contact o2) {
        return o1.getEmail().compareTo(o2.getEmail());
    }

}
