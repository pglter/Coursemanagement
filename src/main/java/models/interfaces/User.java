package models.interfaces;

import java.util.List;

/**
 * Created by Phil on 04.07.2016.
 */
public interface User {

    int getUserId();

    void setUserId(int userId);

    String getName();

    void setName(String name);

    String getVorname();

    void setVorname(String vorname);

    String getMatrikelnr();

    void setMatrikelnr(String matrikelnr);

    String getEmail();

    void setEmail(String email);

    List<Course> getCourseOnUsers();

    void setCourseOnUsers(List<Course> courseOnUsers);

    String getPassword();

    void setPassword(String password);

    String getUsername();

    void setUsername(String username);

}
