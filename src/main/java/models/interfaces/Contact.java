package models.interfaces;

/**
 * Created by Phil on 04.07.2016.
 */
public interface Contact {

    int getContactID();

    void setContactID(int contactID);

    String getVorname();

    void setVorname(String vorname);

    String getName();

    void setName(String name);

    String getEmail();

    void setEmail(String email);

    String toString();

    int compareTo(Contact o);
    int compare(Contact o1,Contact o2);

}
