package core;


/**
 * Dies ist eine Hilfsklasse zum herrausfinden auf welchem BS diese Java VM läuft
 *
 * please keep the notes below as a pseudo-license
 * http://stackoverflow.com/questions/228477/how-do-i-programmatically-determine-operating-system-in-java
 * compare to http://svn.terracotta.org/svn/tc/dso/tags/2.6.4/code/base/common/src/com/tc/util/runtime/Os.java
 * http://www.docjar.com/html/api/org/apache/commons/lang/SystemUtils.java.html
 */

import java.util.Locale;

public final class OsCheck {
    /**
     *  Dieses Enum enthält verschiedene Arten von BS
     */
    public enum OSType {
        Windows, MacOS, Linux, Other
    }

    ;

    // gespeichertes Resultat der BS-Identifizierung
    protected static OSType detectedOS;

    /**
     * This method detect the operating system from the os.name System property and cache the result
     *
     * @return - returns the detected Operating System like MacOS, Winsows or Linux
     */
    public static OSType getOperatingSystemType() {
        if (detectedOS == null) {
            String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
            if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
                detectedOS = OSType.MacOS;
            } else if (OS.indexOf("win") >= 0) {
                detectedOS = OSType.Windows;
            } else if (OS.indexOf("nux") >= 0) {
                detectedOS = OSType.Linux;
            } else {
                detectedOS = OSType.Other;
            }
        }
        return detectedOS;
    }
}