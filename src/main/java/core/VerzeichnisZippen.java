package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Klasse zum Zippen von Verzeichnissen
 * @author Maximilian Stolz
 */

public class VerzeichnisZippen {

    /**
     * Diese Methode ist zum Verzippen für die Backups
     * @param directiory
     */
    public VerzeichnisZippen(String directiory) {

        ZipOutputStream zos = null;
        try {
            File f = new File(directiory + ".zip");
            System.out.println("Erzeuge Archiv " + f.getCanonicalPath());
            zos = new ZipOutputStream(new FileOutputStream(
                    f.getCanonicalPath()));
            zipDir(directiory, directiory, new File(directiory), zos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (zos != null) zos.close();
            } catch (IOException ioe) {
            }
        }
    }

    /**
     * Diese Methode legt alle Dateipfade in ein Array ab mit allen Unterverzeichnissen
     * @param zipName - Neu erstellter Zipname
     * @param dirToZip - Verzeichnissnamepfad
     * @param dirToZipFile - Datei des Verzeichnisses
     * @param zos - Buffer
     */
    private void zipDir(String zipName, String dirToZip, File dirToZipFile,
                        ZipOutputStream zos) {
        if (zipName == null || dirToZip == null || dirToZipFile == null
                || zos == null || !dirToZipFile.isDirectory())
            return; //if there is a mistake happend go out of the method

        FileInputStream fis = null;
        try {
            File[] fileArr = dirToZipFile.listFiles(); //all files in the dir come in a Array
            String path;
            for (File f : fileArr) {
                if (f.isDirectory()) { //if f is dir call the method zipDir again in this method.
                    zipDir(zipName, dirToZip, f, zos);
                    continue;
                }
                fis = new FileInputStream(f);
                path = f.getCanonicalPath();
                String[] name = path.split(dirToZip.substring(2));
                String newName = dirToZip.substring(2) + name[1];
                System.out.println("Packe " + newName);
                zos.putNextEntry(new ZipEntry(newName));
                int len;
                byte[] buffer = new byte[2048];
                while ((len = fis.read(buffer, 0, buffer.length)) > 0) {
                    zos.write(buffer, 0, len);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(fis != null) fis.close();
            } catch (IOException ioe) {}
        }
    }
} 