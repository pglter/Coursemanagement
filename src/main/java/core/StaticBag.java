package core;

import controller.CreateCourseController;
import controller.OverviewController;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import models.classes.CourseHibernateImpl;

import java.io.File;

/**
 * Created by Phil on 13.09.2016.
 */
public class StaticBag {
    public static ObservableList<String> coursesFromUsers = FXCollections.observableArrayList();
    public static ObservableList<String> coursesFromUsersWinter = FXCollections.observableArrayList();
    public static ObservableList<String> coursesFromUsersSummer = FXCollections.observableArrayList();
    public static ObservableList<File> themesList = FXCollections.observableArrayList();

    private OverviewController oc = new OverviewController();

    /**
     * Fügt Kurse der Liste hinzu
     * @param semester
     * @param course
     */
    public static void addCourseToList(String semester, String course) {
        StaticBag.coursesFromUsers.add(course);
        CreateCourseController.setLastCourseCreated(true);
        if (semester.equals(CourseHibernateImpl.SOSE))
            StaticBag.coursesFromUsersSummer.add(course);
        else if (semester.equals(CourseHibernateImpl.WISE))
            StaticBag.coursesFromUsersWinter.add(course);
    }

    /**
     * Gibt die Gesamte Liste an Kursen des Users zurück
     * @return StaticBag.coursesFromUsers
     */
    public static ObservableList<String> getCourseList() {
        return StaticBag.coursesFromUsers;
    }

    /**
     * Gibt die Liste an Wintersemesterkursten des Users zurück
     * @return StaticBag.coursesFromUsersWinter
     */
    public static ObservableList<String> getCoursesFromUsersWinter() {
        return StaticBag.coursesFromUsersWinter;
    }

    /**
     *  Gibt die Liste an Sommersemesterkursten des Users zurück
     * @return StaticBag.coursesFromUsersSummer
     */
    public static ObservableList<String> getCoursesFromUsersSummer() {
        return StaticBag.coursesFromUsersSummer;
    }

    /**
     *  Löscht die gesamte Liste an Kursen des Useres
     */
    public static void deleteList() {
        StaticBag.coursesFromUsers.removeAll();
    }
}
