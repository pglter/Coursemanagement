package controller;

import com.jfoenix.controls.*;
import core.CourseManagement;
import core.StaticBag;
import core.VerzeichnisZippen;
import database.fileDaoImpl;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import gui.Toast;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import database.courseDaoImpl;
import database.userDaoImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import models.classes.CourseHibernateImpl;
import models.classes.FileHibernateImpl;
import models.interfaces.Course;
import models.interfaces.User;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Phil on 31.08.2016.
 */
public class OverviewController {
    @FXML
    private JFXButton uploadBtn;
    @FXML
    private JFXButton chooseFileBtn;
    @FXML
    private ComboBox semesterPicker;
    @FXML
    private ListView<String> listView;
    @FXML
    private ListView<String> listViewScripts;
    @FXML
    private ListView<String> listViewExercises;
    @FXML
    private JFXRadioButton scripts;
    @FXML
    private JFXRadioButton exercises;
    @FXML
    private JFXComboBox courseComboBox;
    @FXML
    private JFXTextField uploadFilePath;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private MenuItem backupBtn;
    @FXML
    private MenuBar menuBar;
    @FXML
    private MenuItem createCourse;
    @FXML
    private MenuItem deleteCourse;
    @FXML
    private MenuItem newMail;
    @FXML
    private Label actualUserLb;
    @FXML
    private Label courseLb;
    @FXML
    private Label gradeLb;
    @FXML
    private Label commentLb;
    @FXML
    private Label currentUserLbl;
    @FXML
    private JFXButton createCourseScnd;

    //------------------- Upload Informationen -----------------//
    private String uplFilePath;
    private String uplCategory;
    private String uplSemester;
    private String uplCourse;
    private File uplFile;
    //----------------------------------------------------------//

    private CourseManagement courseManagement = new CourseManagement();
    private User currentUser;
    private userDaoImpl userDao = new userDaoImpl();
    private courseDaoImpl courseDao = new courseDaoImpl();
    private StructureManager struct = new StructureManager();

    //------------------- String Konstanten -----------------//
    private final static String SOSE = "Sommersemester";
    private final static String WISE = "Wintersemester";
    private final static String ALL_SEMESTERS = "Alle Semester";
    private final static String SCRIPT_CATEGORY = "Skripte";
    private final static String EXERCISE_CATEGORY = "Uebungen";
    //-------------------------------------------------------//


    /**
     * Standard Konstruktor
     * @author Philipp Gölter
     */
    public OverviewController() {
    }

    /**
     * Gets automatically called whenever the scene gets initialized.
     * Note: Steht nicht in der jeweiligen fxml datei drin. Javafx verwaltet das automatisch, aber nur bezüglich der initialize Methode.
     *
     * @author Philipp Gölter
     * @date 16.01.2016
     */
    @FXML
    private void initialize() {
        // Speichere den aktuell eingeloggten User ab
        currentUser = LoginController.getCurrentUserID();
        currentUserLbl.setText(currentUser.getUsername());
        // Überprüfe, ob der User für Kurse eingeschrieben ist
        checkSubscriptions(currentUser);

        // Fülle die Combo Box mit den Semestern und setze handler
        initComboBox(currentUser);

        // Initalisere die Kurslisten in StaticBAg.java für den aktuell angemeldeten User
        initStaticCourseLists(currentUser.getUserId());

        // Fülle die Listview mit den Elementen der Kursliste
        showCoursesFromUser();
        setListenerToLists();

        // Handler festlegen für die Kurse in der Listview
        initCourseView();

        // Initialisere den upload Tab
        initUploadTab();

        // Handler für die MenuBar festlegen
        initMenuBar();
    }

    /**
     * Initialisiert die Menueleiste und setzt die entsprechenden Aktionen fuer die einzelnen Elemente
     * @author Philipp Gölter
     */
    private void initMenuBar() {
        createCourse.setOnAction((ActionEvent event) -> {
            try {
                courseManagement.showNewDialog("Kurs anlegen", "../scenes/createCourse.fxml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        deleteCourse.setOnAction((ActionEvent event) -> {
            try {
                String courseString = listView.getSelectionModel().getSelectedItem().toString();
                Course course = courseDao.getCoursebyName(courseString);
                fileDaoImpl fileDao = new fileDaoImpl();
                for (int i = 0; i < course.getFiles().size(); i++) {
                    fileDao.deleteFile(course.getFiles().get(i));
                }
                courseDao.deleteCourse(course);
                StaticBag.coursesFromUsers.remove(courseString);
                StaticBag.coursesFromUsersSummer.remove(courseString);
                StaticBag.coursesFromUsersWinter.remove(courseString);
                listViewScripts.setItems(null);
                listViewExercises.setItems(null);
                actualUserLb.setText(null);
                commentLb.setText(null);
                courseLb.setText(null);
                gradeLb.setText(null);
                listView.getSelectionModel().clearSelection();
                courseComboBox.getItems().remove(courseString);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        newMail.setOnAction((ActionEvent event) -> {
            try {
                courseManagement.showNewDialog("Mail", "../scenes/Mail.fxml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


    }

    /**
     * Füllt die Combobox mit den Semestern und setzt das erste Element als Standardauswahl.
     * Weiterhin werden auf die Element Actionhandler gesetzt. Es werden je nach Auswahl die Kurse für
     * das entsprechende Semester angezeigt.
     * @param user Depending on the selected item, show course from user.
     * @return boolean - True if succeeded; False otherwise
     * @author Philipp Gölter
     * @date 01.09.2016
     */
    private boolean initComboBox(User user) {

        semesterPicker.getItems().addAll(ALL_SEMESTERS, SOSE, WISE);
        // Set first element as default selected element
        semesterPicker.getSelectionModel().selectFirst();
        if (semesterPicker.getItems() == null)
            return false;
        else
            semesterPicker.setOnAction((e) -> {
                switch (semesterPicker.getSelectionModel().getSelectedIndex()) {
                    case 0:
                        showCoursesFromUser();
                        break;
                    case 1:
                        showCoursesFromUser(SOSE);
                        break;
                    case 2:
                        showCoursesFromUser(WISE);
                        break;
                    default:
                        break;
                }
                System.out.println("Es wurde " + semesterPicker.getSelectionModel().getSelectedItem() + " ausgewählt.");
            });
        return true;
    }

    //---------------- Kursansicht ---------------//
    /**
     * Sets handler to the courseListView
     * @author Philipp Gölter
     */
    private void initCourseView() {
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
               if (event.getClickCount() == 2) {
                    if (listView.getSelectionModel().isEmpty()) {
                        System.out.println("KLICK!");
                    } else {
                        tabPane.getSelectionModel().selectFirst();
                        String selectedCourseString = listView.getSelectionModel().getSelectedItem();
                        Course selectedCourse = courseDao.getCoursebyName(selectedCourseString);
                        initScriptContainer(selectedCourse.getCourseId());
                        initExerciseContainer(selectedCourse.getCourseId());

                        actualUserLb.setText(LoginController.getCurrentUserID().getUsername());
                        commentLb.setText(selectedCourse.getComment());
                        courseLb.setText(selectedCourseString);
                        gradeLb.setText(String.format("%d", selectedCourse.getNote()));
                        listView.getSelectionModel().clearSelection();

                    }
                }
            }
        });
        createCourseScnd.setOnAction((ActionEvent event) -> {
            try {
                courseManagement.showNewDialog("Kurs anlegen", "../scenes/createCourse.fxml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Initialisiert die Listview und zeigt alle Kurse vom übergebenen User an.
     * @author Philipp Ggölter.
     * @date 01.09.2016
     */
    private void showCoursesFromUser() {
        listView.setItems(null);
        // Add all courses to listView element
        listView.setItems(StaticBag.getCourseList());
    }

    /**
     * Initialisiert eine ListView welche alle Kurse eines Users im ausgewählten Semester anzeigt.
     * @param semester SOSE, WISE
     * @author Philipp Gölter
     */
    private void showCoursesFromUser(String semester) {
        listView.setItems(null);
        switch (semester) {
            case CourseHibernateImpl.SOSE:
                listView.setItems(StaticBag.getCoursesFromUsersSummer());
                break;
            case CourseHibernateImpl.WISE:
                listView.setItems(StaticBag.getCoursesFromUsersWinter());
                break;
        }


    }
    //--------------------------------------------//

    /** Für alle Kurse überprüfen ob der übergebene User für diese eingeschrieben ist.
     * Falls dies nicht der Fall ist schreibe den User für alle Kurse ein.
     * @param user
     * @author Philipp Gölter
     */
    private void checkSubscriptions(User user) {
        // All courses the user subscribed to
        List<Course> coursesFromUser = courseDao.getCoursesFromUser(user);

        // All courses registered in the database
        List<Course> allCourses = courseDao.getCourses();
        boolean contained;

        // Subscribe user to all courses he is not subscribed to yet.
        for (Course c : allCourses) {
            contained = false;
            for (Course cUser : coursesFromUser) {
                if (c.getCourseId() == cUser.getCourseId()) {
                    contained = true;
                }
            }
            if (contained == false)
                courseDao.subscribeToCourse(c, user);
        }
    }

    //---------------- Skript-Tab ---------------//
    private void initScriptContainer(int courseID) {
        String filename;
        Course course;
        ObservableList<String> list = FXCollections.observableArrayList();
        ObservableList<String> paths = FXCollections.observableArrayList();
        //courseDaoImpl courseDao = new courseDaoImpl();
        course = courseDao.getCourseById(courseID);
        final String courseName = course.getName();
        List<models.interfaces.File> fileList = course.getFiles();
        for (models.interfaces.File f : fileList) {
            if (f.getKategorie().equals("Skripte")) {
                filename = f.getFilename();
                list.add(filename);
                paths.add(f.getPath());
            }
        }
        listViewScripts.setItems(list);
        listViewScripts.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    if (listViewScripts.getSelectionModel().isEmpty()) {

                    } else {
                        openFile(paths.get(listViewScripts.getSelectionModel().getSelectedIndex()));
                        listViewScripts.getSelectionModel().clearSelection();

                    }
                }
            }
        });
    }
    //--------------------------------------------//

    //---------------- Übungen-Tab ---------------//
    private void initExerciseContainer(int courseID) {
        String filename;
        Course course;

        ObservableList<String> list = FXCollections.observableArrayList();
        ObservableList<String> paths = FXCollections.observableArrayList();

        course = courseDao.getCourseById(courseID);
        final String courseName = course.getName();
        List<models.interfaces.File> fileList = course.getFiles();
        for (models.interfaces.File f : fileList) {
            if (f.getKategorie().equals("Uebungen")) {
                filename = f.getFilename();
                list.add(filename);
                paths.add(f.getPath());
            }
        }
        listViewExercises.setItems(list);
        listViewExercises.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 1) {
                    if (listViewExercises.getSelectionModel().isEmpty()) {
                    } else {
                        openFile(paths.get(listViewExercises.getSelectionModel().getSelectedIndex()));
                        listViewExercises.getSelectionModel().clearSelection();

                    }
                }
            }
        });
    }
    //--------------------------------------------//

    /**
     * Öffne eine PDF-Datei eines Kurses
     *
     * @param path
     */
    private void openFile(String path) {
        try {
            Desktop desktop = Desktop.getDesktop();
            if (desktop != null && desktop.isSupported(Desktop.Action.OPEN)) {
                desktop.open(new File(path));
            } else {
                System.err.println("PDF-Datei kann nicht angezeigt werden!");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    //---------------- Upload-Tab ---------------//
    /**
     * Inizialisiert den Upload-Tab
     */
    private void initUploadTab() {
        uploadBtn.setDisable(true);


        ToggleGroup categories = new ToggleGroup();

        courseComboBox.getItems().addAll(StaticBag.getCourseList());

        scripts.setToggleGroup(categories);
        scripts.setOnAction((ActionEvent event) -> {
            uplCategory = null;
            uplCategory = SCRIPT_CATEGORY;
        });

        exercises.setToggleGroup(categories);
        exercises.setOnAction((ActionEvent event) -> {
            uplCategory = null;
            uplCategory = EXERCISE_CATEGORY;
        });

        chooseFileBtn.setOnAction((ActionEvent event) -> {
            uplFile = null;
            uplFile = openFileChooser();
            try {
                if (uplFile != null)
                    uploadFilePath.setText(uplFile.getAbsolutePath());
                uplFilePath = uplFile.getAbsolutePath();
            } catch (NullPointerException nex) {
                Toast.makeErrorMessage(((Stage) uploadFilePath.getScene().getWindow()), "Es wurde keine Datei ausgewählt!", 3500, 200, 200);
            }
        });

        uploadFilePath.textProperty().addListener((observable, oldValue, newValue) -> {
            if ((exercises.isSelected() || scripts.isSelected()) && (courseComboBox.getSelectionModel().isEmpty() == false))
                uploadBtn.setDisable(false);
        });

        categories.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if ((exercises.isSelected() || scripts.isSelected()) && (courseComboBox.getSelectionModel().isEmpty() == false)
                        && uplFile != null)
                    uploadBtn.setDisable(false);
            }
        });

        courseComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                if ((exercises.isSelected() || scripts.isSelected()) && uplFile != null)
                    uploadBtn.setDisable(false);
            }
        });

        uploadBtn.setOnAction((ActionEvent event) -> {
            Course course = courseDao.getCoursebyName(courseComboBox.getSelectionModel().getSelectedItem().toString());
            FileHibernateImpl fileToUpload = new FileHibernateImpl(course, uplFile.getName(), uplCategory, "", uplFile.getAbsolutePath());
            uploadFile(fileToUpload);
            initScriptContainer(course.getCourseId());
            initExerciseContainer(course.getCourseId());
            listView.getSelectionModel().select(course.getName());

            actualUserLb.setText(LoginController.getCurrentUserID().getUsername());
            commentLb.setText(course.getComment());
            courseLb.setText(course.getName());
            gradeLb.setText(String.format("%d", course.getNote()));
            listView.getSelectionModel().clearSelection();
        });
    }

    /**
     * Öffnet ein Dateiauswahlfenster
     *
     * @return File
     * @author Philipp Gölter
     */
    public File openFileChooser() {
        File selectedFile = null;
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select File to upload");
            selectedFile = fileChooser.showOpenDialog(null);
        } catch (Exception ex) {
            Toast.makeErrorMessage(((Stage) uploadFilePath.getScene().getWindow()), "Es wurde keine Datei ausgewählt!", 3500, 300, 300);
        }
        return selectedFile;
    }

    /**
     * Lädt eine Datei und Kopiert diese zum dafür vorgesehenen Ordner
     * @param file
     * @author Philipp Gölter
     */
    private void uploadFile(models.interfaces.File file) {
        String newPath = struct.copyUpload(currentUser.getName(), file.getCourse().getSemester(), file.getCourse().getName(), file.getKategorie(), file.getPath());
        file.setPath(newPath);
        fileDaoImpl fileDao = new fileDaoImpl();
        fileDao.createUpdateFile(file);


        if(scripts.isSelected())
            tabPane.getSelectionModel().select(0);
        else if(exercises.isSelected())
            tabPane.getSelectionModel().select(1);
        uploadFilePath.setText(null);
        courseComboBox.getSelectionModel().clearSelection();
        scripts.setSelected(false);
        exercises.setSelected(false);

    }
    //--------------------------------------------//

    /**
     * Füllt die Listen in der Klasse StaticBag.java mit den jeweiligen Kursen des Users
     * @param userID
     * @author Philipp Gölter
     */
    private void initStaticCourseLists(int userID) {
        User user;

        // Get user with the ID specified by method parameter
        user = userDao.getUserById(userID);

        // Get a list (Type List<>) of all courses the user subscribed to
        List<Course> coursesFromUser = courseDao.getCoursesFromUser(user);

        // Put all Course names in an ObservableList to use in listView
        for (Course c : coursesFromUser) {
            StaticBag.coursesFromUsers.add(c.getName());
            if (c.getSemester().equals(CourseHibernateImpl.SOSE))
                StaticBag.coursesFromUsersSummer.add(c.getName());
            if (c.getSemester().equals(CourseHibernateImpl.WISE))
                StaticBag.coursesFromUsersWinter.add(c.getName());

        }

    }

    /**
     * Fügt den Listen in StaticBag.java bei Initialisierung der Overview Szene ChangeListener hinzu
     * um zu überprüfen, ob die Liste sich geändert hat
     * @author Philipp Gölter
     */
    private void setListenerToLists() {
        StaticBag.coursesFromUsers.addListener((ListChangeListener) c -> {
            if (courseComboBox.getSelectionModel().getSelectedIndex() == 0)
                showCoursesFromUser();
            courseComboBox.getItems().removeAll(StaticBag.getCourseList());
            courseComboBox.getItems().addAll(StaticBag.getCourseList());
            System.out.println(StaticBag.coursesFromUsersWinter);

            //TODO Falls fehlermeldung bei kurs erstellen hier löschen
            if(CreateCourseController.isLastCourseCreated()) {
                CreateCourseController.setLastCourseCreated(false);
                actualUserLb.setText(LoginController.getCurrentUserID().getUsername());
                commentLb.setText(CreateCourseController.getLastCreatedCourse().getComment());
                courseLb.setText(CreateCourseController.getLastCreatedCourse().getName());
                gradeLb.setText(String.format("%d", CreateCourseController.getLastCreatedCourse().getNote()));

            }
        });

        StaticBag.coursesFromUsersWinter.addListener((ListChangeListener) c -> {
            if (courseComboBox.getSelectionModel().getSelectedIndex() == 1)
                showCoursesFromUser(CourseHibernateImpl.WISE);
            courseComboBox.getItems().removeAll(StaticBag.getCoursesFromUsersWinter());
            courseComboBox.getItems().addAll(StaticBag.getCoursesFromUsersWinter());

        });

        StaticBag.coursesFromUsersSummer.addListener((ListChangeListener) c -> {
            if (courseComboBox.getSelectionModel().getSelectedIndex() == 2)
                showCoursesFromUser(CourseHibernateImpl.SOSE);
            courseComboBox.getItems().removeAll(StaticBag.getCoursesFromUsersSummer());
            courseComboBox.getItems().addAll(StaticBag.getCoursesFromUsersSummer());
        });
    }

    /**
     * Erstellt ein Backup des Kurses der angemeldeten Person
     */
    public void backupClicked() {
        new VerzeichnisZippen("./Kursunterlagenverwaltung");

    }

}
