package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import com.sun.corba.se.spi.orbutil.fsm.Action;
import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import models.classes.ContactHibernateImpl;
import models.interfaces.Contact;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Diese Klasse dient als Kontrollklasse zum Erstellen und abspeichern für Kontakte im Emailverkehr
 */
public class ContactController {
    @FXML
    private JFXButton addButton;
    @FXML
    private JFXButton editButton;
    @FXML
    private JFXButton removeButton;
    @FXML
    private JFXListView<ContactHibernateImpl> contactListView;
    @FXML
    private JFXTextField givenNameTextField;
    @FXML
    private JFXTextField surNameTextField;
    @FXML
    private JFXTextField emailAddressTextField;
    protected ContactList contactList = new ContactList();
    private ObservableList<ContactHibernateImpl> contactObservableList = FXCollections.observableArrayList();

    /**
     * Wird bei der Initialisation automatisch ausgeführt
     */
    @FXML
    private void initialize() {
        contactObservableList.addAll(contactList.getContacts());
        contactListView.setItems(contactObservableList);
        editButton.setDisable(true);
        surNameTextField.setDisable(true);
        addButton.setDisable(true);
        removeButton.setDisable(true);
        givenNameTextField.setFocusTraversable(false);
        emailAddressTextField.setFocusTraversable(false);
        contactListView.setItems(contactObservableList);
        contactListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        contactListView.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<Contact>() {
                                                                               @Override
                                                                               public void onChanged(Change<? extends Contact> c) {
                                                                                   String givenName = "", surName = "", emailAddress = "";
                                                                                   if (contactListView.getSelectionModel().getSelectedItems().size() == 1) {
                                                                                       Contact contact = contactListView.getSelectionModel().getSelectedItem();
                                                                                       givenName = contact.getVorname();
                                                                                       surName = contact.getName();
                                                                                       emailAddress = contact.getEmail();
                                                                                       editButton.setDisable(false);
                                                                                       removeButton.setDisable(false);
                                                                                   } else {
                                                                                       editButton.setDisable(true);
                                                                                       removeButton.setDisable(true);
                                                                                   }
                                                                                   givenNameTextField.setText(givenName);
                                                                                   surNameTextField.setText(surName);
                                                                                   emailAddressTextField.setText(emailAddress);
                                                                               }
                                                                           }

        );
        emailAddressTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            Contact selectedContact = contactListView.getSelectionModel().getSelectedItem();
            if (selectedContact != null && contactListView.getSelectionModel().getSelectedItems().size() == 1 && emailAddressTextField.getText().compareTo(selectedContact.getEmail()) == 0) {
                removeButton.setDisable(false);
            } else {
                removeButton.setDisable(true);
            }
            if (emailAddressTextField.getText().matches("^[\\w._%+-]+@[\\w.-]+\\.[A-Za-z]{2,}$")) {
                addButton.setDisable(false);
                if (selectedContact != null && contactListView.getSelectionModel().getSelectedItems().size() == 1) {
                    editButton.setDisable(false);
                }
            } else {
                addButton.setDisable(true);
                editButton.setDisable(true);
            }
        });
        givenNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            surNameTextField.setDisable(newValue.trim().isEmpty());
        });
    }

    /**
     * Das ist die Aktion die ausgeführt wird wenn auf Edit gedrückt wird
     * @param event
     */
    @FXML
    private void editButtonAction(ActionEvent event) {
        int i = contactListView.getSelectionModel().getSelectedIndex();
        Contact contact = new ContactHibernateImpl(givenNameTextField.getText(), surNameTextField.getText(), emailAddressTextField.getText());
        boolean worked = contactList.editContact(i, contact);
        if (worked) {
            contactObservableList.setAll(contactList.getContacts());
            emptyTextFields();
            refreshListView();
            scrollAndSelect(contactList.whereIsContact(contact.getEmail()));
        }
    }

    /**
     * Das ist die Aktion die ausgeführt wird wenn auf + gedrückt wird
     * @param event
     */
    @FXML
    private void addButtonAction(ActionEvent event) {
        Contact contact = new ContactHibernateImpl(givenNameTextField.getText(), surNameTextField.getText(), emailAddressTextField.getText());
        boolean worked = contactList.addContact(contact);
        if (worked) {
            refreshListView();
            contactListView.scrollTo(contactList.whereIsContact(contact.getEmail()));
            emptyTextFields();
        }
    }

    /**
     * Das ist die Aktion die ausgeführt wird wenn auf - gedrückt wird
     * @param event
     */
    @FXML
    private void removeButtonAction(ActionEvent event) {
        Contact contact = new ContactHibernateImpl(givenNameTextField.getText(), surNameTextField.getText(), emailAddressTextField.getText());
        boolean worked = contactList.removeContact(contact);
        if (worked) {
            emptyTextFields();
            refreshListView();
            removeButton.setDisable(true);
        }
    }

    /**
     * Das ist die Aktion die ausgeführt wird wenn auf Hinzufügen gedrückt wird
     * @param event
     * @throws IOException
     */
    @FXML
    private void addContactsButtonAction(ActionEvent event) throws IOException {
        //Hilfe-Edson
        MailController.receivers.setAll(contactListView.getSelectionModel().getSelectedItems());
        // Nachdem diese gespeichert sind sollen die in der ComboBox hinzugefügt werden mit
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

    private void emptyTextFields() {
        givenNameTextField.setText("");
        surNameTextField.setText("");
        emailAddressTextField.setText("");
        editButton.setDisable(true);
    }

    /**
     * Methode zur Aktualisierung der Anzeige, der Liste
     */
    private void refreshListView() {
        contactObservableList.setAll(contactList.getContacts());
    }

    /**
     * Methode zum auswählen des jeweiligen Models
     * @param i
     */
    private void scrollAndSelect(int i) {
        contactListView.scrollTo(i);
        contactListView.getSelectionModel().select(i);
    }
}

