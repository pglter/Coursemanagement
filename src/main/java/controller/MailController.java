package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import core.StaticBag;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javamail.MailManager;
import javafx.fxml.FXML;
import models.interfaces.Contact;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Die MailController-Klasse bietet funktionen zum erstellen und senden von Emails.
 */
public class MailController {
    @FXML
    private JFXComboBox attachmentComboBox;
    @FXML
    private JFXComboBox receiverComboBox;
    @FXML
    private JFXTextField subjectTextField;
    @FXML
    private JFXTextArea messageTextField;
    @FXML
    private JFXButton sendButton;
    @FXML
    private JFXButton deleteButton;
    private MailManager mailManager = new MailManager();
    private String installationPath = "";

    public static ObservableList<Contact> receivers = FXCollections.observableArrayList();

    /**
     * Wird bei der Initialisation automatisch ausgeführt
     */
    @FXML
    private void initialize() {
        sendButton.setDisable(true);
        attachmentComboBox.setDisable(true);
        receiverComboBox.setDisable(true);
        sendButton.setDisable(true);
        receiverComboBox.setDisable(true);
        deleteButton.setDisable(true);
        setListenerToLists();
    }

    /**
     * Das ist die Aktion die ausgeführt wird wenn auf Senden gedrückt wird
     *
     * @param event
     */
    @FXML
    private void sendButtonAction(ActionEvent event) {
        addReceiversToArrayList();
        setSubject();
        setMessage();
        mailManager.setEmailOfSender("kursunterlagenverwaltung@gmail.com");
        mailManager.setPasswordOfSender("edsonsouza031");
        mailManager.setInstallationPath(installationPath);
        mailManager.sendMail();
        receivers=FXCollections.observableArrayList();
        ((Stage)sendButton.getScene().getWindow()).close();
    }

    /**
     * Das ist die Aktion die ausgeführt wird, wenn auf Anhang(+) gedrückt wird.
     * Dabei kann man Dateien dem Anhang hinzufügen.
     * @param event
     */
    @FXML
    private void attachmentButtonAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Zum Anhang hinzufügen...");
        List<File> files = fileChooser.showOpenMultipleDialog(new Stage());
        if (files != null) {
            mailManager.setZipName("Anhang.zip");
            for (File file : files) {
                mailManager.addNewFile(file.getPath());
                attachmentComboBox.getItems().setAll(mailManager.getListOfFiles());
            }
            attachmentComboBox.getSelectionModel().selectFirst();
            attachmentComboBox.setDisable(false);
            deleteButton.setDisable(false);
        }
    }

    /**
     * Das ist die Aktion die ausgeführt wird, wenn beim Anhand der Butten(-) gedrückt wird.
     * Dabei wird der derzeit ausgewählte Anhang wieder entfernt.
     * @param event
     */
    @FXML
    private void deleteButtonAction(ActionEvent event) {
        ComboBoxFilePathObject selected = (ComboBoxFilePathObject) attachmentComboBox.getValue();
        if (attachmentComboBox.getValue() != null) {
            mailManager.deleteFileSelection(selected.getFilePath());
            attachmentComboBox.getItems().setAll(mailManager.getListOfFiles());
        }
        if (attachmentComboBox.getItems().size() == 0) {
            attachmentComboBox.setDisable(true);
            deleteButton.setDisable(true);
        }
    }

    @FXML
    private void contactsButtonAction(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../scenes/Contact.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Kontaktagenda");
        stage.setScene(new Scene(root, 600, 400));
        stage.show();
    }

    /**
     * Fügt die Empfänger hinzu.
     */
    private void addReceiversToArrayList() {
        for (Contact receiver : receivers) {
            mailManager.addNewContact(receiver.getEmail());
        }
    }

    /**
     * Setzt das Betreff der Mail
     */
    private void setSubject() {
        mailManager.setSubject(subjectTextField.getText());
    }

    /**
     * Setz die Nachricht der Mail
     */
    private void setMessage() {
        mailManager.setEmailText(messageTextField.getText());
    }

    private void setListenerToLists() {
        receivers.addListener((ListChangeListener) c ->
        {
            receiverComboBox.getItems().setAll(receivers);
            if(!receivers.isEmpty()) {
                receiverComboBox.getSelectionModel().select(0);
                sendButton.setDisable(false);
                receiverComboBox.setDisable(false);
            } else
            {
                receiverComboBox.setDisable(true);
                sendButton.setDisable(true);
            }
        });
    }
}
