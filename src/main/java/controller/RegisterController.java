package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import core.CourseManagement;
import database.userDaoImpl;
import gui.Toast;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import models.classes.UserHibernateImpl;
import models.interfaces.User;

/**
 * Created by mikegoebel on 12.09.16.
 */
public class RegisterController {
    @FXML
    private JFXButton speichern;
    @FXML
    private JFXButton abbrechen;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXTextField vorname;
    @FXML
    private JFXTextField nachname;
    @FXML
    private JFXTextField email;
    @FXML
    private JFXTextField matrikelnummer;

    private StructureManager struct = new StructureManager();
    private UserHibernateImpl user = new UserHibernateImpl();
    private User usersaved;
    private userDaoImpl userDao = new userDaoImpl();

    /**
     * Wird automatisch aufgerufen wann immer die Scene initialisiert wird
     * Note: Steht nicht in der jeweiligen fxml datei drin. Javafx verwaltet das automatisch, aber nur bezüglich der initialize Methode
     */
    @FXML
    private void initialize() {
        speichern.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveNewUser();
            }
        });
        abbrechen.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    cancel();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Speichert den neu angelegten User in der Datenbank ab
     */
    void saveNewUser() {
        user = new UserHibernateImpl(username.getText(), nachname.getText(), vorname.getText(), matrikelnummer.getText(), email.getText(), password.getText());
        this.usersaved = userDao.createUpdateUser(user);
        if (this.usersaved != null) {
            Stage stage = (Stage) speichern.getScene().getWindow();
            Toast.makeText(Color.RED, stage, speichern, "Sie wurden erfolgreich regestriert, Sie können sich jetzt anmelden.", 2500, 250, 250);
            ((Stage) speichern.getScene().getWindow()).close();
            System.out.println("User erfolgreich angelegt!");
            struct.createUserIfNotExist(user.getUsername());
        } else {
            Stage stage = (Stage) speichern.getScene().getWindow();
            Toast.makeText(Color.RED, stage, speichern, "Überprüfen Sie bitte die eingegeben Daten! User konnte nicht erstellt werden!", 2500, 250, 250);
            System.out.println("User nicht angelegt!");
        }
    }

    /**
     * Beendet die RegistirerungsScene und geht nach Abbruch auf die LoginScene zurück
     * @throws InterruptedException
     */
    void cancel() throws InterruptedException {
        CourseManagement cm = new CourseManagement();
        Stage stage = (Stage) abbrechen.getScene().getWindow();
        Toast.makeText(Color.RED, stage, abbrechen, "Sie haben abbrechen geklickt und werden nun auf die Login Seite zurück gebracht!", 2500, 250, 250);
        ((Stage) abbrechen.getScene().getWindow()).close();
    }
}
