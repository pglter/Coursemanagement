package controller;

import core.OsCheck;

/**
 * Diese Klasse erstellt ein Pfad-Objekt für beliebige Systeme
 */
public class ComboBoxFilePathObject {
    private String fileName;
    private String filePath;

    /**
     * Dazugehörige Klasse zum erstellen dieses Objektes
     * @param filePath
     */
    public ComboBoxFilePathObject(String filePath) {
        String[] treePath;
        OsCheck.OSType ostype = OsCheck.getOperatingSystemType();
        switch (ostype) {
            case Windows:
                treePath = filePath.split("\\\\");
                break;
            default:
                treePath = filePath.split("/");
                break;
        }

        this.fileName = treePath[treePath.length - 1];
        this.filePath = filePath;
    }

    /**
     * Getter-Methode zum erhalten des Datei-Pfades
     * @return filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * ToString Methode zur Ausgabe
     * @return fileName
     */
    public String toString() {
        return fileName;
    }
}
