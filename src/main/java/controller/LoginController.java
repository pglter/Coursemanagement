package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.sun.javafx.css.StyleManager;
import core.CourseManagement;
import core.StaticBag;
import database.userDaoImpl;
import gui.Toast;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import models.classes.UserHibernateImpl;
import models.interfaces.Course;
import models.interfaces.User;

import javax.print.attribute.standard.JobKOctetsProcessed;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 *Diese Kontroll Klasse dient zur authorisations erkennung zum Zugriff der Kursunterlagenverwaltung
 */
public class LoginController {

    @FXML
    public JFXButton login;
    @FXML
    private JFXButton registrieren;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXComboBox themePicker;

    String userFromtextField;
    String passwdFromTextField;

    private User user;
    private userDaoImpl userDao = new userDaoImpl();
    private static User userShare;
    private final static String ADD_THEME = "CSS-Style hinzufügen...";

    // Global verfügbar um die Themes bei einzelnen Szenen zu laden.
    public static File selectedCssTheme;


    /**
     * Wird automatisch aufgerufen wann immer die Scene initialisiert wird
     * Note: Steht nicht in der jeweiligen fxml datei drin. Javafx verwaltet das automatisch, aber nur bezüglich der initialize Methode
     */
    @FXML private void initialize()
    {
        //initThemePicker();
        //getDefaultThemes();
        login.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loginPerformed();
            }
        });

        registrieren.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                register();
            }
        });
    }

    /**
     * Hier werden die verschiedenen Überprüfungen gemacht wenn der Login-Button gedrückt wird.
     * @return true or false
     */
    public boolean loginPerformed() {
        JFrame frame = new JFrame();
        userFromtextField = username.getText();
        passwdFromTextField = password.getText();
        System.out.print(userFromtextField);
        user = this.userDao.getUserbyName(this.userFromtextField);

        if (user == null) {
            Stage currentStage = (Stage) login.getScene().getWindow();
            Toast.makeErrorMessage( currentStage, "Username nicht gefunden: " + userFromtextField, 2500, 250, 250);
            return false;
        }
        if (user.getPassword().matches(passwdFromTextField)) {
            Stage currentStage = (Stage) login.getScene().getWindow();
            currentStage.close();
            System.out.println("Login erfolgreich!");
            userShare = user;
            CourseManagement cm = new CourseManagement();
            try {
                boolean okay  = cm.showNewDialog("Kursverwaltung", "../scenes/Overview.fxml");
                Toast.makeMessage(currentStage, "Sie wurden erfolgreich angemeldet und werden nun auf die Hauptseite weitergeleitet.", 2500, 250, 250);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            Stage currentStage = (Stage) login.getScene().getWindow();
            Toast.makeErrorMessage( currentStage, "Username oder Passwort falsch! Haben Sie sich schon regestriert?", 2500, 250, 250);
            return false;
        }
    }


    /**
     * Lädt die Standard Themes aus dem Resourceordner und kopiert diese in die Ordnerstruktur
     * @author Philipp Gölter
     */
/*
    private void getDefaultThemes()
    {
        ObservableList<String> defaultThemes = FXCollections.observableArrayList();
        StructureManager struct = new StructureManager();

        //File defThemeOne = new File("../core/Default_Theme_1.css");
        //File defThemeTwo = new File("../resources/Default_Theme_2.css");
        //struct.copyFile(defThemeOne, new File("Kursunterlagenverwaltung/Styles/" + defThemeOne.getName()));
        //struct.copyFile(defThemeTwo, new File("Kursunterlagenverwaltung/Styles/" + defThemeTwo.getName()));
        //StaticBag.themesList.addAll(defThemeOne);
        //defaultThemes.addAll(defThemeOne.getName());
        //themePicker.getItems().addAll(defaultThemes);
        String css = this.getClass().getResource("Default_Theme_1.css").toExternalForm();
        login.getScene().getStylesheets().add(css);
    }
*/
    /**
     * Initialisiert die ComboBox um eigene Designs auszuwählen
     * @author Philipp Gölter
     */
    /*
    private void initThemePicker()
    {
        ObservableList<String> themes = FXCollections.observableArrayList();
        themes.add(ADD_THEME);
        themePicker.getItems().addAll(themes);

        themePicker.setOnAction((e) -> {
            if(themePicker.getSelectionModel().getSelectedItem().equals(ADD_THEME)) {
                File newFile = openFileChooser();
                if(newFile == null)
                    themePicker.getSelectionModel().clearSelection();

                String fileName = newFile.getName();

                if(fileName.endsWith(".css"))
                {
                    themePicker.getItems().add(fileName);
                    selectedCssTheme = newFile;
                } else
                {
                    Stage stage = (Stage) login.getScene().getWindow();
                    Toast.makeText(Color.RED, stage,login,"Bitte ein .css File auswählen!", 3500,500,500);
                }
                themePicker.getSelectionModel().clearSelection();
            }
            else
                {
                    //TODO: Ausgewähltes css file laden
                }

        });

    }
*/
    /**
     * Öffnet die Scene um Dateien auszuwählen und zu speichern
     * @author Philipp Gölter
     * @return File
     */
    private File openFileChooser()
    {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Theme");
        File selectedFile = fileChooser.showOpenDialog(null);
        return selectedFile;
    }

    /**
     * Erstellt ein neues Kursunterlagenverwaltungsobjekt für den neuen User
     */
    public void register(){
        CourseManagement cm = new CourseManagement();
        try {
            boolean okay = cm.showNewDialog("Neuen User Registrieren","../scenes/Register.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Standart get Methode um die ID des Users herrauszufinden
     * @return userShare
     */
    public static User getCurrentUserID(){
        return userShare;
    }
}
