package database;

import models.interfaces.File;
import org.hibernate.Session;

/**
 * Created by mikegoebel on 18.07.16.
 */
public class fileDaoImpl {
    public fileDaoImpl() {
    }

    public File createUpdateFile(File file) {
        Session s = null;
        try {
            s = SessionUtil.getSession();
            s.beginTransaction();
            s.saveOrUpdate(file);
            s.getTransaction().commit();

            return file;
        } catch (Exception ex) {
            s.getTransaction().rollback();
            System.out.println("Upload hat nicht funktioniert!");
        } finally {
            s.close();
        }
        return null;
    }

    public File deleteFile(File file) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        s.delete(file);
        s.getTransaction().commit();
        s.close();
        return file;
    }
}
